//
//  GameLayer.h
//  Basketball 01
//
//  Created by shourav on Sep 2016.
//  Copyright (c) 2016 Navid Hasan. All rights reserved.
//


#import "cocos2d.h"
#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>
#import <GoogleMobileAds/GADBannerView.h>

#define kTagFacebookAlert   101
#define kTagInstagramAlert  102
#define kTagTwitterAlert    103
#define kTagRateAlert       104

typedef enum {
    FacebookShareTypeGetFreeValues,
    FacebookShareTypeShareImages
}   FacebookShareType;

typedef enum {
    GetFreeSkips,
    GetFreeReveals,
    GetFreeRemoves
}   GetFreeValues;

@interface GameLayer : CCLayer <UIDocumentInteractionControllerDelegate, SKProductsRequestDelegate, SKPaymentTransactionObserver, UIAlertViewDelegate,GADBannerViewDelegate> {
    
    SKProductsRequest * productRequest;
    SKProduct * product;
    
    UIActivityIndicatorView *uploadingView;
    BOOL isUploading;

    CCMenu *letterBtnMenu;
    CCMenu *menu;
    CCMenu *letterMenu;
    
    CCMenu *menuSkipShop;
    CCMenu *menuRevealShop;
    CCMenu *menuRemoveShop;
    
    CCLayer *shopLayer;
    
    NSMutableArray *puzzleLabelArray;
    NSMutableArray *letterLabelArray;
    NSMutableArray *puzzleBackArray;
    NSMutableArray *letterArray;
    NSMutableArray *inputedArray;
    NSMutableArray *revealArray;
    NSMutableArray *signArray;;
    CCSprite *puzzleSprite;
    
    CCLabelTTF *levelLabel;
    CCLabelTTF *questionLabel;
    
    CCLabelTTF *skipCountLabel;
    CCLabelTTF *revealCountLabel;
    CCLabelTTF *removeCountLabel;
    
    int currentPos;
    
    NSString *strpuzzleName;
    NSString *strInputedName;
    
    NSMutableArray *indexArray;
    
    CCMenu *closeMenu;
    
    CCLabelTTF *shopTitleLabel;
    NSMutableArray *shopLabelArray;
    
    CCLayer *winLayer;
    CCLabelTTF *foundpuzzleLabel;
    CCLabelTTF *puzzleNumberLabel;
    
    CCLayer *levelClearLayer;
    CCLabelTTF *levelClearLabel;
    
    GADBannerView *AbMob;

}

@property ( retain ) SKProductsRequest * productRequest;
@property ( retain ) SKProduct * product;
+(GameLayer*) getInstance;
+ (CCScene*) scene;

-(void)hidFreeBtn;

@end
