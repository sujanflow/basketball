//
//  Global.h
//  Basketball 01
//
//  Created by shourav on Sep 2016.
//  Copyright (c) 2016 Navid Hasan. All rights reserved.
//

#import "cocos2d.h"

// iOS Version Checking
#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

//shared application
#define APP_DELEGATE ((AppController *)[[UIApplication sharedApplication] delegate])

//App Link
//rate
#define RATE_LINK_IOS7 @"itms-apps://itunes.apple.com/app/id1031048322"
#define RATE_LINK_IOS6 @"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=1031048322"

//App Information of Identifiers
#define kAppleID    @"1031048322"
#define kAppName    @"Guess The Basketball Player Quiz"
#define kSKU        @"Basket"
#define kBundleID   @"com.sandbox.basketball"

//admob app ID
#define ADMOB_ID @"ca-app-pub-3702023121137364/2928397130"
#define ADS_FREQUENCY 2

//URLS
//#define kUrlAppIcon             @"http://allo-apps.com/wp-content/uploads/2013/10/flage_.png"
//#define kUrlTestContentImage    @"http://allo-apps.com/wp-content/uploads/2013/08"
#define kUrlApplicationItunes   @"https://itunes.apple.com/app/id1031048322?ls=1&mt=8"
#define kUrlShortenedAppItune   @"https://itunes.apple.com/app/id1031048322?ls=1&mt=8"

//PUZZLE PLAY LIST
#define kPuzzleImagePlistName   @"puzzlelist"

//IN APP PURCHASE KEYS
#define Skip1 @"com.sandbox.basketball.3skips"
#define Skip2 @"com.sandbox.basketball.9skips"
#define Skip3 @"com.sandbox.basketball.20skips"
#define Skip4 @"com.sandbox.basketball.60skips"

#define Reveal1 @"com.sandbox.basketball.6reveals"
#define Reveal2 @"com.sandbox.basketball.18reveals"
#define Reveal3 @"com.sandbox.basketball.40reveals"
#define Reveal4 @"com.sandbox.basketball.100reveals"

#define Remoev1 @"com.sandbox.basketball.4removes"
#define Remoev2 @"com.sandbox.basketball.12removes"
#define Remoev3 @"com.sandbox.basketball.30removes"
#define Remoev4 @"com.sandbox.basketball.80removes"


extern NSMutableArray *puzzleArray;
extern int questionNumber;
extern BOOL showAds;
extern BOOL g_bFacebookLogined;
extern int sound;
extern int deletedCount;

extern int skipItemCount;
extern int revealItemCount;
extern int removeItemCount;

extern int skipShopCount[4];
extern int revealShopCount[4];
extern int removeShopCount[4];

extern float skipShopPrice[4];
extern float revealShopPrice[4];
extern float removeShopPrice[4];

extern int freeSkip;
extern int freeReveal;
extern int freeRemove;

extern int fbFlag;
extern int isSaved;

float getPositionX(float x);
float getPositionY(float y);
float getX(float x);
float getY(float y);