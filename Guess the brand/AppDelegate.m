//
//  AppDelegate.m
//  Basketball 01
//
//  Created by shourav on Sep 2016.
//  Copyright (c) 2016 Navid Hasan. All rights reserved.
//


#import "cocos2d.h"
#import "AppDelegate.h"
#import "MainLayer.h"
#import "GameLayer.h"
#import "Global.h"
#import <Chartboost/Chartboost.h>
#import <CommonCrypto/CommonDigest.h>
#import <AdSupport/AdSupport.h>

@implementation AppController

@synthesize window=window_, navController=navController_, director=director_;
@synthesize cb;

static AppController *m_appController;
+(AppController*) getInstance {
    return m_appController;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{

// Initialize the Chartboost library
    [Chartboost startWithAppId:@"55cfbf5d0d60256eccd36739"
                  appSignature:@"63509eb8cc4d91c1f15cb9405e460dbfb9eab1ea"
                      delegate:self];
    
//    self.cb = [Chartboost sharedChartboost];
//    self.cb.delegate=self;
    
//    self.cb.appId        = kChartBoostAppID;
//    self.cb.appSignature = kChartBoostAppSignature;
    
//    [self.cb startSession];
//    [self.cb showInterstitial];
    
    m_appController  = self;

    NSUserDefaults *prefs   = [NSUserDefaults standardUserDefaults];
    showAds                 = [prefs boolForKey:@"showAds"];
    g_bFacebookLogined      = [prefs boolForKey:@"isFacebook"];
    questionNumber          = [prefs integerForKey:@"number"];
    skipItemCount           = [prefs integerForKey:@"skip"];
    revealItemCount         = [prefs integerForKey:@"reveal"];
    removeItemCount         = [prefs integerForKey:@"remove"];
    isSaved                 = [prefs integerForKey:@"isSaved"];
    freeSkip                = [prefs integerForKey:@"freeSkip"];
    freeReveal              = [prefs integerForKey:@"freeReveal"];
    freeRemove              = [prefs integerForKey:@"freeRemove"];

    int first = [prefs integerForKey:@"first"];     //if application is opened first time
    if(first == 0) {
        skipItemCount   = 1;
        revealItemCount = 1;
        removeItemCount = 1;
        
        [prefs setInteger:1 forKey:@"first"];
        [prefs setInteger:skipItemCount forKey:@"skip"];
        [prefs setInteger:revealItemCount forKey:@"reveal"];
        [prefs setInteger:removeItemCount forKey:@"remove"];
    }
    
    //Local Notifications
    UILocalNotification *notification = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    if ( notification.userInfo != nil ) {
        
        NSInteger number = [notification.userInfo[@"randomNumber"] integerValue];
        if (number == 0) {
            revealItemCount = revealItemCount + 2;
            [prefs setInteger:revealItemCount forKey:@"reveal"];
        } else if (number == 1) {
            removeItemCount = removeItemCount + 2;
            [prefs setInteger:removeItemCount forKey:@"remove"];
        } else if (number == 2) {
            skipItemCount = skipItemCount + 1;
            [prefs setInteger:skipItemCount forKey:@"skip"];
        }
        
        application.applicationIconBadgeNumber = 0;
    }
    
    [prefs synchronize];
    
    NSString * puzzleFilePath = [[NSBundle mainBundle] pathForResource:kPuzzleImagePlistName ofType:@"plist"];
    puzzleArray = [[NSMutableArray alloc] initWithContentsOfFile:puzzleFilePath];
    
// Create the main window
	window_ = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];

    // Create an CCGLView with a RGB565 color buffer, and a depth buffer of 0-bits
	CCGLView *glView = [CCGLView viewWithFrame:[window_ bounds]
								   pixelFormat:kEAGLColorFormatRGB565	//kEAGLColorFormatRGBA8
								   depthFormat:0	//GL_DEPTH_COMPONENT24_OES
							preserveBackbuffer:NO
									sharegroup:nil
								 multiSampling:NO
							   numberOfSamples:0];

	director_ = (CCDirectorIOS*) [CCDirector sharedDirector];
	director_.wantsFullScreenLayout = YES;

	// Display FSP and SPF
	//[director_ setDisplayStats:YES];

	// set FPS at 60
	[director_ setAnimationInterval:1.0/60];

	// attach the openglView to the director
	[director_ setView:glView];

	// for rotation and other messages
	[director_ setDelegate:self];

	// 2D projection
	[director_ setProjection:kCCDirectorProjection2D];
    [director_ setProjection:kCCDirectorProjection3D];

	// Enables High Res mode (Retina Display) on iPhone 4 and maintains low res on all other devices
	if( ! [director_ enableRetinaDisplay:YES] )
		CCLOG(@"Retina Display Not supported");

	// Default texture format for PNG/BMP/TIFF/JPEG/GIF images
	// It can be RGBA8888, RGBA4444, RGB5_A1, RGB565
	// You can change anytime.
	[CCTexture2D setDefaultAlphaPixelFormat:kCCTexture2DPixelFormat_RGBA8888];

	// If the 1st suffix is not found and if fallback is enabled then fallback suffixes are going to searched. If none is found, it will try with the name without suffix.
	// On iPad HD  : "-ipadhd", "-ipad",  "-hd"
	// On iPad     : "-ipad", "-hd"
	// On iPhone HD: "-hd"
	CCFileUtils *sharedFileUtils = [CCFileUtils sharedFileUtils];
	[sharedFileUtils setEnableFallbackSuffixes:NO];				// Default: NO. No fallback suffixes are going to be used
	[sharedFileUtils setiPhoneRetinaDisplaySuffix:@"-hd"];		// Default on iPhone RetinaDisplay is "-hd"
	[sharedFileUtils setiPadSuffix:@"-ipad"];					// Default on iPad is "ipad"
	[sharedFileUtils setiPadRetinaDisplaySuffix:@"-ipadhd"];	// Default on iPad RetinaDisplay is "-ipadhd"

	// Assume that PVR images have premultiplied alpha
	[CCTexture2D PVRImagesHavePremultipliedAlpha:YES];

	// and add the scene to the stack. The director will run it when it automatically when the view is displayed.
	[director_ pushScene: [MainLayer scene]];

	
	// Create a Navigation Controller with the Director
	navController_ = [[UINavigationController alloc] initWithRootViewController:director_];
	navController_.navigationBarHidden = YES;
	
	// set the Navigation Controller as the root view controller
    // [window_ addSubview:navController_.view];	// Generates flicker.
	[window_ setRootViewController:navController_];
	
	// make main window visible
	[window_ makeKeyAndVisible];
	
	return YES;
}

// Supported orientations: Landscape. Customize it for your own needs
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

// getting a call, pause the game
-(void) applicationWillResignActive:(UIApplication *)application {
	if( [navController_ visibleViewController] == director_ )
		[director_ pause];
}

// call got rejected
-(void) applicationDidBecomeActive:(UIApplication *)application {
	if( [navController_ visibleViewController] == director_ )
		[director_ resume];
    
    [[UIApplication sharedApplication ] cancelAllLocalNotifications];
    application.applicationIconBadgeNumber = 0;
}

-(void)showChartboost {
    [self.cb showInterstitial];
}

-(void) applicationDidEnterBackground:(UIApplication*)application {
	if( [navController_ visibleViewController] == director_ )
		[director_ stopAnimation];
    
    NSArray *arrayOfMessages = @[@"Basketball Quiz Game is missing you! Play now to get 2 Reveal Letters Free!",
                                 @"Basketball Quiz Game is missing you! Play now to get 2 Remove Letters Free!",
                                 @"Basketball Quiz Game is missing you! Play now to get 1 Skip Level Free!"];
    
    [[UIApplication sharedApplication ] cancelAllLocalNotifications];
    application.applicationIconBadgeNumber = 0;
    
    NSString *answer = nil;
    if(questionNumber <= puzzleArray.count) {
        answer = [puzzleArray objectAtIndex:questionNumber];
        
        //Local Notifications
        [self setNotyficationsSchedule:[NSString stringWithFormat:@"Don't give up yet! Here's a hint: the answer starts with %@",           [answer substringToIndex:1]]
                         intervalHours:1 userInfo:nil];
        
        [self setNotyficationsSchedule:[NSString stringWithFormat:@"Did you give up? The answer is %@!", answer]
                         intervalHours:12 userInfo:nil];
    }
    
        NSNumber *randomNumber = [NSNumber numberWithInteger:[self randIndexForArrayCounts:3]];
        NSDictionary *userInfo = @{ @"randomNumber" : randomNumber };
    
        [self setNotyficationsSchedule:[arrayOfMessages objectAtIndex:[randomNumber integerValue]]
                         intervalHours:2*12 userInfo:userInfo];
    
        [self setNotyficationsSchedule:@"Basketball Quiz Game is missing you! Play Now to get free Reveals!"
                         intervalHours:3*12 userInfo:nil];
}

-(void) applicationWillEnterForeground:(UIApplication*)application {
	if( [navController_ visibleViewController] == director_ )
		[director_ startAnimation];
}

// application will be killed
- (void)applicationWillTerminate:(UIApplication *)application {
	CC_DIRECTOR_END();
}

// purge memory
- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
	[[CCDirector sharedDirector] purgeCachedData];
}

// next delta time will be zero
-(void) applicationSignificantTimeChange:(UIApplication *)application {
	[[CCDirector sharedDirector] setNextDeltaTimeZero:YES];
}

- (void) dealloc
{
	[window_ release];
	[navController_ release];

	[super dealloc];
}

#pragma mark Chartboost Delegate

/*
 * Chartboost Delegate Methods
 *
 * Recommended for everyone: shouldDisplayInterstitial
 *
 * shouldDisplayInterstitial
 *
 * This is used to control when an interstitial should or should not be displayed
 * The default is YES, and that will let an interstitial display as normal
 * If it's not okay to display an interstitial, return NO
 *
 * For example: during gameplay, return NO.
 *
 * Is fired on:
 * -Interstitial is loaded & ready to display
 */

- (BOOL)shouldDisplayInterstitial:(CBLocation)location {
    
    // For example:
    // if the user has left the main menu and is currently playing your game, return NO;
    
    // Otherwise return YES to display the interstitial
    return YES;
}

    // Called when an interstitial has failed to come back from the server
- (void)didFailToLoadInterstitial:(CBLocation)location  withError:(CBLoadError) error    {

    //[GADInterstitial showOver:[[UIApplication sharedApplication] keyWindow]];
}

/*
 * didCacheInterstitial
 *
 * Passes in the location name that has successfully been cached.
 *
 * Is fired on:
 * - All assets loaded
 * - Triggered by cacheInterstitial
 *
 * Notes:
 * - Similar to this is: cb.hasCachedInterstitial(String location)
 * Which will return true if a cached interstitial exists for that location
 */

- (void)didCacheInterstitial:(CBLocation)location {
    
}

/*
 * didFailToLoadMoreApps
 *
 * This is called when the more apps page has failed to load for any reason
 *
 * Is fired on:
 * - No network connection
 * - No more apps page has been created (add a more apps page in the dashboard)
 * - No publishing campaign matches for that user (add more campaigns to your more apps page)
 *  -Find this inside the App > Edit page in the Chartboost dashboard
 */

- (void)didFailToLoadMoreApps {
    NSLog(@"failure to load more apps");
    
    [GADInterstitial showOver:[[UIApplication sharedApplication] keyWindow]];

}

/*
 * didDismissInterstitial
 *
 * This is called when an interstitial is dismissed
 *
 * Is fired on:
 * - Interstitial click
 * - Interstitial close
 *
 * #Pro Tip: Use the delegate method below to immediately re-cache interstitials
 */

- (void)didDismissInterstitial:(CBLocation)location {
    
    [[Chartboost sharedChartboost] cacheInterstitial:location];
}

/*
 * didDismissMoreApps
 *
 * This is called when the more apps page is dismissed
 *
 * Is fired on:
 * - More Apps click
 * - More Apps close
 *
 * #Pro Tip: Use the delegate method below to immediately re-cache the more apps page
 */

- (void)didDismissMoreApps {
    NSLog(@"dismissed more apps page, re-caching now");
    
    [Chartboost showMoreApps:CBLocationDefault];
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    if (notification.userInfo) {
        NSUserDefaults *prefs   = [NSUserDefaults standardUserDefaults];
        NSInteger number = [notification.userInfo[@"randomNumber"] integerValue];
        if (number == 0) {
            revealItemCount = revealItemCount + 2;
            [prefs setInteger:revealItemCount forKey:@"reveal"];
        } else if (number == 1) {
            removeItemCount = removeItemCount + 2;
            [prefs setInteger:removeItemCount forKey:@"remove"];
        } else if (number == 2) {
            skipItemCount = skipItemCount + 1;
            [prefs setInteger:skipItemCount forKey:@"skip"];
        }
        [prefs synchronize];
    }
}

/*
 * shouldRequestInterstitialsInFirstSession
 *
 * This sets logic to prevent interstitials from being displayed until the second startSession call
 *
 * The default is NO, meaning that it will always request & display interstitials.
 * If your app displays interstitials before the first time the user plays the game, implement this method to return NO.
 */

- (BOOL)shouldRequestInterstitialsInFirstSession {
    
    return YES;
}

-(void)setNotyficationsSchedule:(NSString *)message intervalHours:(NSInteger)intervalHour userInfo: (NSDictionary *) userInfo
{
    NSDate *curretDate = [NSDate date];
    NSTimeInterval currentTimeInterval = [curretDate timeIntervalSince1970]; //This current Time in SECOND
    
    NSTimeInterval scheduleInterval = (60 * 60 * intervalHour);
    
    NSDate *sheduleDate = [NSDate dateWithTimeIntervalSince1970:scheduleInterval + currentTimeInterval];
    
    UILocalNotification* localNotification = [[UILocalNotification alloc] init];
    localNotification.fireDate = sheduleDate;
    localNotification.alertBody = message;
    
    localNotification.timeZone = [NSTimeZone defaultTimeZone];
    localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
    localNotification.userInfo = userInfo;
    
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}

-(int)randIndexForArrayCounts:(int)count
{
    int r = arc4random() % count;
    NSLog(@"RAND : %d",r);
    return r;
}


@end

