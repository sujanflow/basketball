//
//  AppDelegate.h
//  Basketball 01
//
//  Created by shourav on Sep 2016.
//  Copyright (c) 2016 Navid Hasan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "cocos2d.h"
#import "AdManager.h"
#import <Chartboost/Chartboost.h>

@interface AppController : NSObject <UIApplicationDelegate, CCDirectorDelegate,ChartboostDelegate>
{
	UIWindow *window_;
	UINavigationController *navController_;

	CCDirectorIOS	*director_;							// weak ref
    Chartboost *cb;
    
    NSArray* _permissions;

}

@property (nonatomic, retain) UIWindow *window;
@property (readonly) UINavigationController *navController;
@property (readonly) CCDirectorIOS *director;
@property (nonatomic, retain) Chartboost *cb;

+(AppController*) getInstance;

-(void) initFacebook;
-(void) SendQuoteToFacebook:(int)index :(int)number;
-(void) loginFacebook;
-(void) showChartboost;

@end
