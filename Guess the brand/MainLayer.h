//
//  MainLayer.h
//  Basketball 01
//
//  Created by shourav on Sep 2016.
//  Copyright (c) 2016 Navid Hasan. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import <MessageUI/MessageUI.h>
#import <Chartboost/Chartboost.h>

@interface MainLayer : CCLayer <MFMailComposeViewControllerDelegate, UIAlertViewDelegate, ChartboostDelegate>
{
    //  CCMenu *menu;
    CCLabelTTF *levelLabel;
    CCLabelTTF *questionLevel;
    CCLabelTTF *soundLabel;
}

+ (CCScene*) scene;

@end
