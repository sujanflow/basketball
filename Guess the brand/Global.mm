//
//  Global.mm
//  Basketball 01
//
//  Created by shourav on Sep 2016.
//  Copyright (c) 2016 Navid Hasan. All rights reserved.
//

#include "Global.h"

NSMutableArray *puzzleArray;
NSMutableArray *correctAnswerArray;
NSMutableArray *wrongAnswerArray1;
NSMutableArray *wrongAnswerArray2;
NSMutableArray *wrongAnswerArray3;

int freeSkip;
int freeReveal;
int freeRemove;

int questionNumber;
BOOL showAds;
BOOL g_bFacebookLogined;
int sound;
int deletedCount;

int skipItemCount;
int revealItemCount;
int removeItemCount;

int isSaved;

int skipShopCount[4] = {3,9,20,60};
int revealShopCount[4] = {6,18,40,100};
int removeShopCount[4] = {4,12,30,80};

float skipShopPrice[4]={0.99, 1.99, 4.99, 9.99};
float revealShopPrice[4]={0.99, 1.99, 4.99, 9.99};
float removeShopPrice[4]={0.99, 1.99, 4.99, 9.99};

int fbFlag;
float getPositionX(float x)
{
    CCDirectorIOS	*director_ = (CCDirectorIOS*) [CCDirector sharedDirector];
    CGSize winSize = [director_ winSize];

    return x*winSize.height/1024;
}

float getPositionY(float y)
{
    CCDirectorIOS	*director_ = (CCDirectorIOS*) [CCDirector sharedDirector];
    CGSize winSize = [director_ winSize];

    return y*winSize.width/768;
}

float getX(float x)
{
    CCDirectorIOS	*director_ = (CCDirectorIOS*) [CCDirector sharedDirector];
    CGSize winSize = [director_ winSize];
    
    return x*winSize.width/768;
}

float getY(float y)
{
    CCDirectorIOS	*director_ = (CCDirectorIOS*) [CCDirector sharedDirector];
    CGSize winSize = [director_ winSize];
    
    return y*winSize.height/1024;
}