//
//  AdManager.h
//  Basketball 01
//
//  Created by shourav on Sep 2016.
//  Copyright (c) 2016 Navid Hasan. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <GoogleMobileAds/GADBannerView.h>
#import <GoogleMobileAds/GADInterstitial.h>
#import <GoogleMobileAds/GADInterstitialDelegate.h>

#define admob_bottom_banner @"ca-app-pub-3702023121137364/2928397130"
#define admob_interstitial @"ca-app-pub-3702023121137364/4405130338"

@interface AdManager : NSObject<GADInterstitialDelegate>

+ (id)sharedInstance;

-(void)showAdmobFullscreen;
-(void)showAdmobSplahAd;
-(GADBannerView*)adMobBannerWithAdUnitID:(NSString*)adUnitID andOrigin:(CGPoint)origin;
- (GADRequest *)adMobrequest;

@property(nonatomic, strong) GADInterstitial *interstitial;

@end
