//
//  MainLayer.mm
//  Basketball 01
//
//  Created by shourav on Sep 2016.
//  Copyright (c) 2016 Navid Hasan. All rights reserved.
//


#import "MainLayer.h"
#import "Global.h"
#import "GameLayer.h"
#import "AppDelegate.h"
#import "SimpleAudioEngine.h"

@implementation MainLayer

+ (CCScene*) scene {
    CCScene *scene = [CCScene node];
    [scene addChild:[MainLayer node]];
    return scene;
}

-(id)init
{
    if(self = [super init])
    {
        CCSprite *back = [CCSprite spriteWithFile:@"mainback.png"];
        back.position = ccp(getX(384),getY(512));
        [self addChild:back];
        
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        {
            if([CCDirector sharedDirector].winSize.height > 480)
                back.scaleY = 1136/960.0;
        }
        
        CCSprite *title = [CCSprite spriteWithFile:@"title.png"];
        title.position = ccp(getX(384), getY(770));
        [self addChild:title];
        
        
        //Button's
        
        CCMenuItemImage *playBtn = [CCMenuItemImage itemWithNormalImage:@"playbtn_nml.png" selectedImage:@"playbtn_act.png" target:self selector:@selector(onPlayBtn)];
        playBtn.position = ccp(getX(384), getY(360));
        
        CCMenuItemImage *freegamesBtn = [CCMenuItemImage itemWithNormalImage:@"freegames_nml.png" selectedImage:@"freegamesbtn_act.png" target:self selector:@selector(onFreegamesBtn)];
        freegamesBtn.position = ccp(getX(384), getY(250));
        
        CCMenuItemImage *rateBtn = [CCMenuItemImage itemWithNormalImage:@"ratebtn_nml.png" selectedImage:@"ratebtn_act.png" target:self selector:@selector(onRateBtn)];
        rateBtn.position = ccp(getX(384), getY(150));
        
        CCMenuItemImage *soundBtn = [CCMenuItemImage itemWithNormalImage:@"soundon_nml.png" selectedImage:@"soundon_act.png" target:self selector:@selector(onSoundBtn)];
        soundBtn.position = ccp(getX(384),getY(855));
        
        CCMenuItemImage *emailBtn = [CCMenuItemImage itemWithNormalImage:@"emailbtn_nml.png" selectedImage:@"emailbtn_act.png" target:self selector:@selector(onEmailBtn)];
        emailBtn.position = ccp(getX(284),getY(670));
        
        CCMenuItemImage *fbBtn = [CCMenuItemImage itemWithNormalImage:@"fbbtn_nml.png" selectedImage:@"fbbtn_act.png" target:self selector:@selector(onFacebookBtn)];
        fbBtn.position = ccp(getX(384),getY(670));
        
        CCMenuItemImage *restartBtn = [CCMenuItemImage itemWithNormalImage:@"restartbtn_nml.png" selectedImage:@"restartbtn_act.png" target:self selector:@selector(onRestartBtn)];
        restartBtn.position = ccp(getX(484),getY(670));
        
        CCMenu *menu = [CCMenu menuWithItems:playBtn, freegamesBtn, rateBtn, emailBtn, fbBtn, soundBtn, restartBtn, nil];
        
        menu.position = ccp(0,0);
        [self addChild:menu z:1];
        
        CCSprite *redBack = [CCSprite spriteWithFile:@"levelmark.png"];
        redBack.position = ccp(getX(384),getY(512));
        [self addChild:redBack];
        
        CCSprite *soundback = [CCSprite spriteWithFile:@"soundback.png"];
        soundback.position = ccp(getX(460),getY(855));
        [self addChild:soundback];
        
        levelLabel = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"Level %d", questionNumber / 10 + 1] fontName:@"AmericanTypewriter-Bold" fontSize:getY(51)];
        levelLabel.position = ccp(getX(384),getY(980));
        [self addChild:levelLabel];
        
        questionLevel = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%d", questionNumber + 1] fontName:@"AmericanTypewriter-Bold" fontSize:getY(60)];
        questionLevel.position = ccp(getX(384),getY(512));
        [self addChild:questionLevel];
        
        soundLabel = [CCLabelTTF labelWithString:@"Off" fontName:@"AmericanTypewriter-Bold" fontSize:getY(30)];
        soundLabel.position = ccp(getX(460),getY(855));
        [self addChild:soundLabel];
        
        if(sound == 1)
            [soundLabel setString:@"Off"];
        else
            [soundLabel setString:@"On"];
    }
    
    return self;
}


#pragma mark - alert view delegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 101) {
        if(buttonIndex == 0) {
            
            NSUserDefaults *prefs   = [NSUserDefaults standardUserDefaults];
            
            
            [prefs setInteger:0 forKey:@"rate"];
            [prefs synchronize];
            
            
            questionNumber = 0;
            
            [prefs setInteger:questionNumber forKey:@"number"];
            [prefs synchronize];
            
            [self onPlayBtn];
        }
        return;
    }
    
    if([alertView.title isEqualToString:@"Restart"])
    {
        if(buttonIndex == 0) {
            
            questionNumber  = 0;
            skipItemCount   = 2;
            revealItemCount = 2;
            removeItemCount = 2;
            
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            
            [prefs setInteger:questionNumber forKey:@"number"];
            [prefs setInteger:skipItemCount forKey:@"skip"];
            [prefs setInteger:revealItemCount forKey:@"reveal"];
            [prefs setInteger:removeItemCount forKey:@"remove"];
            [prefs synchronize];
            [self onPlayBtn];
        }
        else {
            
            [Chartboost showInterstitial:CBLocationHomeScreen];
            
        }
    }
}


#pragma mark - button selectors

-(void)onPlayBtn {
    if(sound == 0)
        [[SimpleAudioEngine sharedEngine] playEffect:@"menu_button.mp3"];
    
    if(questionNumber >= puzzleArray.count) {
        UIAlertView *view = [[UIAlertView alloc] initWithTitle:nil
                                                       message:NSLocalizedString(@"amPuzzleCompletedHome", nil)
                                                      delegate:self
                                             cancelButtonTitle:NSLocalizedString(@"yes", nil)
                                             otherButtonTitles:NSLocalizedString(@"no", nil), nil];
        view.tag = 101;
        [view show];
        [view release];
        return;
    }
    
    CCScene *homeScene = (CCScene*)[[GameLayer alloc] init];
    CCTransitionScene *trans = [[CCTransitionFade alloc] initWithDuration:0.5f scene:homeScene];
    [[CCDirector sharedDirector] pushScene: trans];
    [trans release];
    [homeScene release];
}

-(void)onFreegamesBtn {
    if(sound == 0)
        [[SimpleAudioEngine sharedEngine] playEffect:@"menu_button.mp3"];
    
    [Chartboost showMoreApps:CBLocationHomeScreen];
    
}

-(void)onRateBtn {
    if(sound == 0)
        [[SimpleAudioEngine sharedEngine] playEffect:@"menu_button.mp3"];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://itunes.apple.com/app/id1031048322"]];
    
    //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=1031048322"]];
}

-(void)onSoundBtn {
    if(sound == 0){
        [[SimpleAudioEngine sharedEngine] playEffect:@"menu_button.mp3"];
        sound = 1;
        [soundLabel setString:@"Off"];
    }
    else {
        sound = 0;
        [soundLabel setString:@"On"];
    }
    
    [[AdManager sharedInstance] showAdmobFullscreen];
    
}

-(void)onRestartBtn {
    if(sound == 0)
        [[SimpleAudioEngine sharedEngine] playEffect:@"menu_button.mp3"];
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Restart"
                                                        message:@"Do you really want to restart the game?"
                                                       delegate:self
                                              cancelButtonTitle:@"Restart"
                                              otherButtonTitles:@"Cancel", nil];
    [alertView show];
    [alertView release];
    
    [Chartboost showInterstitial:CBLocationHomeScreen];
    
}

#pragma mark - sharing

-(void)onFacebookBtn {
    if(sound == 0)
        [[SimpleAudioEngine sharedEngine] playEffect:@"menu_button.wav"];
    
    AppController *app = [AppController getInstance];
    if(g_bFacebookLogined == NO)
    {
        fbFlag = 0;
        [app loginFacebook];
    }
    else
        [app SendQuoteToFacebook:0 :questionNumber];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"fb://profile/133132756896614"]];
}

-(void)onEmailBtn {
    if(sound == 0)
        [[SimpleAudioEngine sharedEngine] playEffect:@"menu_button.wav"];
    
    if( [MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailController = [[[MFMailComposeViewController alloc] init] autorelease];
        mailController.navigationBar.barStyle = UIBarStyleBlack;
        mailController.mailComposeDelegate = self;
        [mailController setSubject:NSLocalizedString(@"AppTagLine", nil)];
        [mailController setMessageBody:[NSString stringWithFormat:NSLocalizedString(@"socialShareMessageEmail", nil), kUrlApplicationItunes] isHTML:NO];
        
        NSString * iconPath = [[NSBundle mainBundle] pathForResource:@"Icon" ofType:@"png"];
        NSData * iconData = [NSData dataWithContentsOfFile:iconPath];
        [mailController addAttachmentData:iconData mimeType:@"image/png" fileName:kAppName];
        
        [[CCDirector sharedDirector] presentModalViewController:mailController animated:YES];
    }
    else {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:NSLocalizedString(@"amEmailError", nil)
                                                           delegate:nil
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil, nil];
        [alertView show];
        [alertView release];
    }
}

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    [[CCDirector sharedDirector] dismissModalViewControllerAnimated:YES];
}

@end
