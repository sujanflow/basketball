//
//  GameLayer.m
//  Basketball 01
//
//  Created by shourav on Sep 2016.
//  Copyright (c) 2016 Navid Hasan. All rights reserved.
//


#import "GameLayer.h"
#import "Global.h"
#import "MainLayer.h"
#import "AppDelegate.h"
#import <Twitter/Twitter.h>
#import "SimpleAudioEngine.h"
#import "iRate.h"

@interface GameLayer () {
    
}
- (void) completeTransaction: (SKPaymentTransaction *)transaction;
- (void) restoreTransaction: (SKPaymentTransaction *)transaction;
- (void) failedTransaction: (SKPaymentTransaction *)transaction;

@property ( retain ) NSString * strpuzzleName;

@end

@implementation GameLayer

@synthesize productRequest;
@synthesize product;
@synthesize strpuzzleName;


+ (CCScene*) scene {
    CCScene *scene = [CCScene node];
    [scene addChild:[GameLayer node]];
    return scene;
}

static GameLayer *m_gamelayer;
+(GameLayer*) getInstance {
    return m_gamelayer;
}

- (id)init {
    if(self = [super init]) {
        m_gamelayer = self;
        
        //initialize indicator
        uploadingView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];// initWithFrame:CGRectMake(0,0 , getX(50), getX(50))];
        uploadingView.transform = CGAffineTransformMakeScale(2, 2);
        [uploadingView setCenter:ccp(getX(384), getY(512))];
        [uploadingView setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
        
        //initialize director
        CCDirector *director = [CCDirector sharedDirector];
        [[director view] addSubview:uploadingView];
        uploadingView.hidden = YES;
        isUploading = NO;
        
        // set payment observer
        [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
        
        // UI Design
        CCSprite *back = [CCSprite spriteWithFile:@"mainback.png"];
        back.position = ccp(getX(384),getY(512));
        [self addChild:back];
        
        CCSprite *picback = [CCSprite spriteWithFile:@"picback.png"];
        picback.position = ccp(getX(384), getY(646));
        [self addChild:picback];
        
        // GameLayer Buttoon's
        CCMenuItemImage *skipBtn = [CCMenuItemImage itemWithNormalImage:@"skipbtn_nml.png" selectedImage:@"skipbtn_act.png" target:self selector:@selector(onSkipBtn)];
        skipBtn.position = ccp(getX(700), getY(790));
        
        CCMenuItemImage *revealBtn = [CCMenuItemImage itemWithNormalImage:@"revealbtn_nml.png" selectedImage:@"revealbtn_act.png" target:self selector:@selector(onRevealBtn)];
        revealBtn.position = ccp(getX(700), getY(650));
        
        CCMenuItemImage *deleteBtn = [CCMenuItemImage itemWithNormalImage:@"deletebtn_nml.png" selectedImage:@"deletebtn_act.png" target:self selector:@selector(onDeleteBtn)];
        deleteBtn.tag = 1;
        deleteBtn.position = ccp(getX(700),getY(510));
        
        CCMenuItemImage *facebookBtn = [CCMenuItemImage itemWithNormalImage:@"facebookbtn_nml.png"        selectedImage:@"facebookbtn_act.png" target:self selector:@selector(onFacebookBtn)];
        facebookBtn.position = ccp(getX(50), getY(790));
        
        CCMenuItemImage *tweetBtn = [CCMenuItemImage itemWithNormalImage:@"tweetbtn_nml.png" selectedImage:@"tweetbtn_act.png" target:self selector:@selector(onTweetBtn)];
        tweetBtn.position = ccp(getX(50),getY(650));
        
        CCMenuItemImage *instagramBtn = [CCMenuItemImage itemWithNormalImage:@"instagrambtn_nml.png" selectedImage:@"instagrambtn_act.png" target:self selector:@selector(onInstagramBtn)];
        instagramBtn.position = ccp(getX(50),getY(510));
        
        
        CCMenuItemImage *backBtn = [CCMenuItemImage itemWithNormalImage:@"backbtn_nml.png" selectedImage:@"backbtn_act.png" target:self selector:@selector(onBackBtn)];
        backBtn.position = ccp(getX(100),getY(980));
        
        menu = [CCMenu menuWithItems: skipBtn, revealBtn, deleteBtn, backBtn, facebookBtn, tweetBtn, instagramBtn, nil];
        
        menu.position = ccp(0,0);
        [self addChild:menu];
        
        CCSprite *numberBack = [CCSprite spriteWithFile:@"levelmark.png"];
        numberBack.position = ccp(getX(700),getY(980));
        numberBack.scale = 0.4;
        [self addChild:numberBack];
        
        levelLabel = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"Level %d", questionNumber / 10 + 1] fontName:@"AmericanTypewriter-Bold" fontSize:getY(50)];
        levelLabel.position = ccp(getX(384),getY(980));
        [self addChild:levelLabel];
        
        questionLabel = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%d", questionNumber + 1] fontName:@"AmericanTypewriter-Bold" fontSize:getY(30)];
        questionLabel.position = ccp(getX(700),getY(980));
        [self addChild:questionLabel];
        
        skipCountLabel = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%d", skipItemCount] fontName:@"AmericanTypewriter-Bold" fontSize:getY(21)];
        skipCountLabel.position = ccp(getX(733),getY(790));
        [self addChild:skipCountLabel];
        
        revealCountLabel = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%d", revealItemCount] fontName:@"AmericanTypewriter-Bold" fontSize:getY(21)];
        revealCountLabel.position = ccp(getX(733),getY(650));
        [self addChild:revealCountLabel];
        
        removeCountLabel = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%d", removeItemCount] fontName:@"AmericanTypewriter-Bold" fontSize:getY(21)];
        removeCountLabel.position = ccp(getX(733),getY(510));
        [self addChild:removeCountLabel];
        
        //initialize arrays
        puzzleBackArray     = [[NSMutableArray alloc] initWithObjects: nil];
        puzzleLabelArray    = [[NSMutableArray alloc] initWithObjects: nil];
        letterLabelArray    = [[NSMutableArray alloc] initWithObjects: nil];
        letterArray         = [[NSMutableArray alloc] initWithObjects: nil];
        inputedArray        = [[NSMutableArray alloc] initWithObjects: nil];
        indexArray          = [[NSMutableArray alloc] initWithObjects: nil];
        revealArray         = [[NSMutableArray alloc] initWithObjects: nil];
        signArray           = [[NSMutableArray alloc] initWithObjects: nil];
        
        
        letterBtnMenu = [CCMenu menuWithItems:nil];
        float startPosX; float letterWidth = getX(105.0f); float startPosY;
        
        if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
            
            startPosY = getY(250);
        }
        else {
            startPosY = getY(250);
        }
        
        startPosX = getX(384) - (letterWidth*3);
        for(int i = 0 ; i < 14; i++) {
            if(i == 7) {
                startPosX = getX(384) - (letterWidth*3);
                startPosY -= letterWidth;
            }
            CCMenuItemImage *btn = [CCMenuItemImage itemWithNormalImage:@"letterbtn_nml.png" selectedImage:@"letterbtn_act.png" target:self selector:@selector(onLetterBtns:)];
            btn.position = ccp(startPosX + letterWidth*(i%7), startPosY);
            btn.tag = i;
            [letterBtnMenu addChild:btn];
            
            CCLabelTTF *label = [CCLabelTTF labelWithString:@"A" fontName:@"Helvetica-Bold" fontSize:getY(45)];
            label.position = ccp(startPosX + letterWidth*(i%7), startPosY + getY(5));
            label.color = ccc3(0,0,0);
            [self addChild:label z:1];
            [letterLabelArray addObject:label];
        }
        
        letterBtnMenu.position = ccp(0,0);
        [self addChild:letterBtnMenu];
        [self setLettersAndButtons:questionNumber :1];
        
        //Shop
        shopLayer = [[CCLayer alloc] init];
        
        CCSprite *shopBack = [CCSprite spriteWithFile:@"shopback.png"];
        shopBack.position = ccp(getX(384),getY(512));
        [shopLayer addChild:shopBack];
        
        shopTitleLabel = [CCLabelTTF labelWithString:@"Skip Puzzle" fontName:@"AmericanTypewriter-Bold" fontSize:getY(50)];
        shopTitleLabel.position = ccp(getX(384),getY(860));
        shopTitleLabel.color = ccc3(0,0,0);
        [shopLayer addChild:shopTitleLabel];
        
        startPosY = getY(750.0f); float space = getY(120.0f);
        
        //skip puzzle menu
        menuSkipShop = [CCMenu menuWithItems: nil];
        for(int i = 0; i < 4; i++) {
            CCMenuItemImage *btn = [CCMenuItemImage itemWithNormalImage:@"skipshopbtn_nml.png" selectedImage:@"skipshopbtn_act.png" target:self selector:@selector(onSkipShopBtns:)];
            btn.position = ccp(getX(384), startPosY - space*i);
            btn.tag = i;
            [menuSkipShop addChild:btn];
        }
        menuSkipShop.position = ccp(0,0);
        [shopLayer addChild:menuSkipShop];
        
        //reveal puzzle menu
        menuRevealShop = [CCMenu menuWithItems: nil];
        
        for(int i = 0; i < 4; i++) {
            CCMenuItemImage *btn = [CCMenuItemImage itemWithNormalImage:@"revealshopbtn_nml.png" selectedImage:@"revealshopbtn_act.png" target:self selector:@selector(onRevealShopBtns:)];
            btn.position = ccp(getX(384), startPosY - space*i);
            btn.tag = i;
            [menuRevealShop addChild:btn];
        }
        
        menuRevealShop.position = ccp(0,0);
        [shopLayer addChild:menuRevealShop];
        
        
        //remove puzzle menu
        menuRemoveShop = [CCMenu menuWithItems: nil];
        
        for(int i = 0; i < 4; i++) {
            CCMenuItemImage *btn = [CCMenuItemImage itemWithNormalImage:@"deleteshopbtn_nml.png" selectedImage:@"deleteshopbtn_act.png" target:self selector:@selector(onRemoveShopBtns:)];
            btn.position = ccp(getX(384), startPosY - space*i);
            btn.tag = i;
            [menuRemoveShop addChild:btn];
        }
        
        menuRemoveShop.position = ccp(0,0);
        [shopLayer addChild:menuRemoveShop];
        
        
        //free skip puzzle menu
        CCMenuItemImage *freeSkip = [CCMenuItemImage itemWithNormalImage:@"shopfreebtn_nml.png" selectedImage:@"shopfreebtn_act.png" target:self selector:@selector(onFreeBtn)];
        freeSkip.tag = 0;
        freeSkip.position = ccp(getX(384), startPosY - space*4);
        
        CCMenuItemImage *closeBtn = [CCMenuItemImage itemWithNormalImage:@"cancelbtn_nml.png" selectedImage:@"cancelbtn_act.png" target:self selector:@selector(onShopCloseBtn)];
        closeBtn.position = ccp(getX(384), startPosY - space*4);
        
        closeMenu = [CCMenu menuWithItems:closeBtn, nil];
        closeMenu.position = ccp(0, 0);
        [shopLayer addChild:closeMenu];
        
        shopLabelArray = [[NSMutableArray alloc] initWithObjects: nil];
        
        for(int i = 0; i < 4; i++) {
            CCLabelTTF *lbl = [CCLabelTTF labelWithString:@"+25" fontName:@"AmericanTypewriter-Bold" fontSize:getY(30)];
            lbl.position = ccp(getX(265), startPosY - space*i);
            [shopLayer addChild:lbl];
            [shopLabelArray addObject:lbl];
        }
        
        for(int i = 0; i < 4; i++) {
            CCLabelTTF *lbl = [CCLabelTTF labelWithString:@"$1.99" fontName:@"AmericanTypewriter-Bold" fontSize:getY(30)];
            lbl.position = ccp(getX(550), startPosY - space*i);
            [shopLayer addChild:lbl];
            [shopLabelArray addObject:lbl];
        }
        
        [self addChild:shopLayer z:2];
        shopLayer.visible = NO;
        
        winLayer = [[CCLayer alloc] init];
        
        CCSprite *winBack = [CCSprite spriteWithFile:@"solveback.png"];
        winBack.position = ccp(getX(384),getY(512));
        [winLayer addChild:winBack];
        
        foundpuzzleLabel = [CCLabelTTF labelWithString:@"" fontName:@"AmericanTypewriter-Bold" fontSize:getY(50)];
        foundpuzzleLabel.position = ccp(getX(384),getY(610));
        foundpuzzleLabel.color = ccc3(255,0,0);
        [winLayer addChild:foundpuzzleLabel];
        
        CCSprite *markBack = [CCSprite spriteWithFile:@"levelmark.png"];
        markBack.position = ccp(getX(384), getY(480));
        [winLayer addChild:markBack];
        
        puzzleNumberLabel = [CCLabelTTF labelWithString:@"" fontName:@"AmericanTypewriter-Bold" fontSize:getY(50)];
        puzzleNumberLabel.position = ccp(getX(384),getY(480));
        [winLayer addChild:puzzleNumberLabel];
        
        // Puzzel Solve Button's
        CCMenuItemImage *moreBtn = [CCMenuItemImage itemWithNormalImage:@"moregamebtn_nml.png" selectedImage:@"moregamebtn_act.png" target:self selector:@selector(onMoreBtn)];
        moreBtn.position = ccp(getX(384), getY(320));
        
        CCMenuItemImage *continueBtn = [CCMenuItemImage itemWithNormalImage:@"continuebtn_nml.png" selectedImage:@"continuebtn_act.png" target:self selector:@selector(onContinueBtn)];
        continueBtn.position = ccp(getX(384), getY(210));
        
        CCMenu *winMenu = [CCMenu menuWithItems: moreBtn, continueBtn, nil];
        
        
        winMenu.position = ccp(0,0);
        [winLayer addChild:winMenu];
        [self addChild:winLayer z:2];
        winLayer.visible = NO;
        
        
        levelClearLayer = [[CCLayer alloc] init];
        CCSprite *levelBack = [CCSprite spriteWithFile:@"levelwinback.png"];
        levelBack.position = ccp(getX(384),getY(512));
        [levelClearLayer addChild:levelBack];
        
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            if([CCDirector sharedDirector].winSize.height > 480) {
                back.scaleY = 1136/960.0;
                shopBack.scaleY = 1136/960.0;
                winBack.scaleY = 1136/960.0;
                levelBack.scaleY = 1136/960.0;
            }
        }
        
        levelClearLabel = [CCLabelTTF labelWithString:@"" fontName:@"AmericanTypewriter-Bold" fontSize:getY(80)];
        levelClearLabel.position = ccp(getX(384),getY(715));
        levelClearLabel.color = ccc3(255,0,0);
        [levelClearLayer addChild:levelClearLabel];
        
        
        // Level Win Button's
        
        CCMenuItemImage *downloadBtn = [CCMenuItemImage itemWithNormalImage:@"downloadbtn_nml.png" selectedImage:@"downloadbtn_act.png" target:self selector:@selector(onDownloadBtn)];
        downloadBtn.position = ccp(getX(384), getY(320));
        
        CCMenuItemImage *levelContinueBtn = [CCMenuItemImage itemWithNormalImage:@"continuebtn_nml.png" selectedImage:@"continuebtn_act.png" target:self selector:@selector(onLevelContinueBtn)];
        levelContinueBtn.position = ccp(getX(384), getY(210));
        
        CCMenu *levelMenu = [CCMenu menuWithItems: levelContinueBtn, downloadBtn, nil];
        
        levelMenu.position = ccp(0,0);
        [levelClearLayer addChild:levelMenu];
        [self addChild:levelClearLayer z:2];
        levelClearLayer.visible = NO;
        
        
        AbMob = [[GADBannerView alloc] initWithAdSize:kGADAdSizeSmartBannerPortrait];
        AbMob.delegate=self;
        AppController *delegate = (AppController *)[[UIApplication sharedApplication] delegate];
        AbMob.rootViewController=delegate.navController;
        AbMob.adUnitID =ADMOB_ID ;
        [[[CCDirector sharedDirector] view] addSubview:AbMob];
        GADRequest *r = [[GADRequest alloc] init];
        //r.testing = YES;
        //r.testDevices = [NSArray arrayWithObjects:GAD_SIMULATOR_ID, nil];
        [AbMob loadRequest:r];
        
    }
    return self;
}


#pragma mark - Support

-(void) shareToFacebookWithImage : (UIImage *) image text : (NSString *) text type : (FacebookShareType) shareType{
    //To send data above 6.0
    SLComposeViewController *mySLComposerSheet = nil;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"6.0")) {
        if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) //check if Facebook Account is linked
        {
            mySLComposerSheet = [[SLComposeViewController alloc] init]; //initiate the Social Controller
            mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook]; //Tell him with what social plattform to use it, e.g. facebook or twitter
            [mySLComposerSheet addImage:image];
            [mySLComposerSheet setInitialText:text];
            [APP_DELEGATE.navController presentViewController:mySLComposerSheet animated:YES completion:nil];
        }else{
            UIAlertView *alertView = [[UIAlertView alloc]
                                      initWithTitle:@"Sorry"
                                      message:@"You can't post right now, make sure your device has an internet connection and you have at least one facebook account setup"
                                      delegate:self
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil];
            alertView.tag = kTagFacebookAlert;
            [alertView show];
        }
        
        [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
            NSString *output;
            switch (result) {
                case SLComposeViewControllerResultCancelled:
                    output = @"Action Cancelled!";
                    break;
                case SLComposeViewControllerResultDone:
                    switch (shareType) {
                        case FacebookShareTypeGetFreeValues:
                            [self sharingCompletedAdjustFreeValues];
                            break;
                        case FacebookShareTypeShareImages:
                            break;
                        default:
                            break;
                    }
                    output = @"Post Successfull!";
                    break;
                default:
                    break;
            } //check if everythig worked properly. Give out a message on the state.
        }];
        return;
    }
    
    //To send data below 6.0
    if(g_bFacebookLogined == NO) {
        fbFlag = 1;
        [APP_DELEGATE loginFacebook];
    }
    else {
        [APP_DELEGATE SendQuoteToFacebook:1 :questionNumber];
    }
}

-(void) sharingCompletedAdjustFreeValues {
    if(fbFlag == 2) {
        skipItemCount++;
        fbFlag = 1;
        freeSkip = 1;
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [prefs setInteger:skipItemCount forKey:@"skip"];
        [prefs setInteger:1 forKey:@"freeSkip"];
        [prefs synchronize];
        [[GameLayer getInstance] hidFreeBtn];
    }
    else if(fbFlag == 3) {
        revealItemCount++;
        fbFlag = 1;
        freeReveal = 1;
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [prefs setInteger:revealItemCount forKey:@"reveal"];
        [prefs setInteger:1 forKey:@"freeReveal"];
        [prefs synchronize];
        [[GameLayer getInstance] hidFreeBtn];
    }
    else if(fbFlag == 4) {
        removeItemCount++;
        fbFlag = 1;
        freeRemove = 1;
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [prefs setInteger:1 forKey:@"freeRemove"];
        [prefs setInteger:removeItemCount forKey:@"remove"];
        [prefs synchronize];
        [[GameLayer getInstance] hidFreeBtn];
    }
}

-(void) hidFreeBtn {
    [closeMenu getChildByTag:0].visible = NO;
    [skipCountLabel setString:[NSString stringWithFormat:@"%d", skipItemCount]];
    [revealCountLabel setString:[NSString stringWithFormat:@"%d", revealItemCount]];
    [removeCountLabel setString:[NSString stringWithFormat:@"%d", removeItemCount]];
}


#pragma mark - Button Selectors

- (void)onFreeBtn {
    if(sound == 0)
        [[SimpleAudioEngine sharedEngine] playEffect:@"menu_button.wav"];
    
    if(menuSkipShop.visible == YES) {
        fbFlag = 2;
    }
    else if(menuRevealShop.visible == YES) {
        fbFlag = 3;
    }
    else if(menuRemoveShop.visible == YES){
        fbFlag = 4;
    }
    
    
    //share icon image
    [self shareToFacebookWithImage:[UIImage imageNamed:@"Icon.png"]
                              text:[NSString stringWithFormat: NSLocalizedString(@"jointheFunMessage", nil), kUrlShortenedAppItune]
                              type:FacebookShareTypeGetFreeValues];
}

- (void)onDownloadBtn {
    if(sound == 0)
        [[SimpleAudioEngine sharedEngine] playEffect:@"menu_button.wav"];
    
    [Chartboost showMoreApps:CBLocationHomeScreen];
}

- (void)onMoreBtn {
    if(sound == 0)
        [[SimpleAudioEngine sharedEngine] playEffect:@"menu_button.wav"];
    
    [Chartboost showMoreApps:CBLocationHomeScreen];
}

- (void)onBackBtn {
    if(sound == 0)
        [[SimpleAudioEngine sharedEngine] playEffect:@"menu_button.wav"];
    
    [Chartboost showInterstitial:CBLocationHomeScreen];
    
    deletedCount = 0;
    
    CCScene *homeScene = (CCScene*)[[MainLayer alloc] init];
    CCTransitionScene *trans = [[CCTransitionFade alloc] initWithDuration:0.5f scene:homeScene];
    [[CCDirector sharedDirector] pushScene: trans];
    [trans release];
    [homeScene release];
    
}

- (void)onContinueBtn {
    
    if(sound == 0)
        [[SimpleAudioEngine sharedEngine] playEffect:@"menu_button.wav"];
    
    questionNumber++;
    if(questionNumber >= puzzleArray.count) {
        
        UIAlertView *view = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"atWellDone", nil)
                                                       message:NSLocalizedString(@"amPuzzleCompleted", nil)
                                                      delegate:self
                                             cancelButtonTitle:NSLocalizedString(@"yes", nil)
                                             otherButtonTitles:NSLocalizedString(@"no", nil), nil];
        [view show];
        [view release];
        return;
    }
    
    [self setLettersAndButtons:questionNumber :0];
    winLayer.visible = NO;
}

- (void)onLevelContinueBtn {
    
    if(sound == 0)
        [[SimpleAudioEngine sharedEngine] playEffect:@"menu_button.wav"];
    
    questionNumber++;
    if(questionNumber >= puzzleArray.count) {
        
        UIAlertView *view = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"atWellDone", nil)
                                                       message:NSLocalizedString(@"amPuzzleCompleted", nil)
                                                      delegate:self
                                             cancelButtonTitle:NSLocalizedString(@"yes", nil)
                                             otherButtonTitles:NSLocalizedString(@"no", nil), nil];
        [view show];
        [view release];
        return;
    }
    
    [self setLettersAndButtons:questionNumber :0];
    levelClearLayer.visible = NO;
}

- (void)onLetterBack:(id)sender {
    if(sound == 0)
        [[SimpleAudioEngine sharedEngine] playEffect:@"back_letter.wav"];
    
    CCMenuItemImage *btn = (CCMenuItemImage*)sender;
    
    int index = [[inputedArray objectAtIndex:btn.tag] intValue];
    
    if(index != -1) {
        CCLabelTTF *lbl = [puzzleLabelArray objectAtIndex:btn.tag];
        [lbl setString:@""];
        
        [letterBtnMenu getChildByTag:index].visible = YES;
        lbl = [letterLabelArray objectAtIndex:index];
        lbl.visible = YES;
        
        [inputedArray replaceObjectAtIndex:btn.tag withObject:[NSNumber numberWithInt:-1]];
        
        currentPos--;
        
        for(CCLabelTTF *lbl in puzzleLabelArray)
        {
            lbl.color = ccc3(255,255,255);
        }
        
        for(int i = 0; i < [revealArray count]; i++)
        {
            CCLabelTTF *lbl = [puzzleLabelArray objectAtIndex:[[revealArray objectAtIndex:i] intValue]];
            lbl.color = ccc3(0,0,0);
        }
    }
}

- (void)onLetterBtns:(id)sender {
    
    if(sound == 0)
        [[SimpleAudioEngine sharedEngine] playEffect:@"letter_button.mp3"];
    
    if(currentPos == [strpuzzleName length])
        return;
    
    CCMenuItemImage *imgBtn = (CCMenuItemImage*)sender;
    
    int pos = 0;
    
    for(int i = 0; i < [strpuzzleName length]; i++) {
        if([[inputedArray objectAtIndex:i] intValue] == -1) {
            pos = i;
            break;
        }
    }
    
    CCLabelTTF *label = [puzzleLabelArray objectAtIndex:pos];
    
    NSString *letter = [letterArray objectAtIndex:imgBtn.tag];
    [label setString:letter];
    currentPos++;
    
    label = [letterLabelArray objectAtIndex:imgBtn.tag];
    imgBtn.visible = NO;
    label.visible = NO;
    [inputedArray replaceObjectAtIndex:pos withObject:[NSNumber numberWithInt:imgBtn.tag]];
    
    if(currentPos == [strpuzzleName length]) {
        NSString *str = @"";
        for(int i = 0; i < [strpuzzleName length]; i++) {
            str = [str stringByAppendingString:[letterArray objectAtIndex:[[inputedArray objectAtIndex:i] intValue]]];
        }
        
        if([str isEqualToString:strpuzzleName]) {
            [self showWinScreen];
        }
        else {
            if(sound == 0)
                [[SimpleAudioEngine sharedEngine] playEffect:@"fail.wav"];
            
            for(CCLabelTTF *lbl in puzzleLabelArray)
                lbl.color = ccc3(255,0,0);
            
            for(int i = 0; i < [revealArray count]; i++)
            {
                CCLabelTTF *lbl = [puzzleLabelArray objectAtIndex:[[revealArray objectAtIndex:i] intValue]];
                lbl.color = ccc3(0,0,0);
            }
        }
    }
}


#pragma mark - Handle Suggestions

- (void)onDeleteBtn {
    
    if(sound == 0)
        [[SimpleAudioEngine sharedEngine] playEffect:@"menu_button.wav"];
    
    if(removeItemCount == 0) {
        if(freeRemove == 1)
            [closeMenu getChildByTag:0].visible = NO;
        
        menuSkipShop.visible = NO;
        menuRemoveShop.visible = YES;
        menuRevealShop.visible = NO;
        shopLayer.visible = YES;
        
        [shopTitleLabel setString:@"Remove Letters"];
        
        for(int i = 0; i < 4; i++)
        {
            CCLabelTTF *lbl = [shopLabelArray objectAtIndex:i];
            [lbl setString:[NSString stringWithFormat:@"%d", removeShopCount[i]]];
        }
        
        for(int i = 4; i < 8; i++)
        {
            CCLabelTTF *lbl = [shopLabelArray objectAtIndex:i];
            [lbl setString:[NSString stringWithFormat:@"%.2f$", removeShopPrice[i - 4]]];
        }
        return;
    }
    
    if([strpuzzleName length] <= 13 && deletedCount < 2) {
        
        removeItemCount--;
        [removeCountLabel setString:[NSString stringWithFormat:@"%d", removeItemCount]];
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [prefs setInteger:questionNumber forKey:@"number"];
        [prefs setInteger:skipItemCount forKey:@"skip"];
        [prefs setInteger:revealItemCount forKey:@"reveal"];
        [prefs setInteger:removeItemCount forKey:@"remove"];
        [prefs synchronize];
        
        int n = 0;
        for(int i = 0; i < 14; i++)
        {
            int num = [[indexArray objectAtIndex:i] intValue];
            if(num >= [strpuzzleName length])
            {
                n++;
                [indexArray replaceObjectAtIndex:i withObject:[NSNumber numberWithInt:-1]];
                
                if([letterBtnMenu getChildByTag:i].visible == YES)
                {
                    [letterBtnMenu getChildByTag:i].visible = NO;
                    CCLabelTTF *label = [letterLabelArray objectAtIndex:i];
                    label.visible = NO;
                }
                else
                {
                    for(int j = 0; j < [strpuzzleName length]; j++)
                    {
                        if([[inputedArray objectAtIndex:j] intValue] == i)
                        {
                            [[puzzleLabelArray objectAtIndex:j] setString:@""];
                            [inputedArray replaceObjectAtIndex:j withObject:[NSNumber numberWithInt:-1]];
                            currentPos--;
                            break;
                        }
                    }
                }
            }
            
            if(n == (14-[strpuzzleName length])/2 && deletedCount == 0)
            {
                break;
            }
        }
        
        deletedCount++;
        
        if([strpuzzleName length] == 13)
        {
            deletedCount = 2;
        }
        
        if(currentPos < [strpuzzleName length])
        {
            for(CCLabelTTF *lbl in puzzleLabelArray)
            {
                lbl.color = ccc3(255,255,255);
            }
            
            for(int i = 0; i < [revealArray count]; i++)
            {
                CCLabelTTF *lbl = [puzzleLabelArray objectAtIndex:[[revealArray objectAtIndex:i] intValue]];
                lbl.color = ccc3(0,0,0);
            }
        }
    }
}

- (void)onRevealBtn {
    
    if(sound == 0)
        [[SimpleAudioEngine sharedEngine] playEffect:@"menu_button.wav"];
    
    if(revealItemCount == 0) {
        if(freeReveal == 1)
            [closeMenu getChildByTag:0].visible = NO;
        
        menuSkipShop.visible = NO;
        menuRemoveShop.visible = NO;
        menuRevealShop.visible = YES;
        shopLayer.visible = YES;
        
        [shopTitleLabel setString:@"Reveal Letter"];
        
        for(int i = 0; i < 4; i++)
        {
            CCLabelTTF *lbl = [shopLabelArray objectAtIndex:i];
            [lbl setString:[NSString stringWithFormat:@"%d", revealShopCount[i]]];
        }
        
        for(int i = 4; i < 8; i++)
        {
            CCLabelTTF *lbl = [shopLabelArray objectAtIndex:i];
            [lbl setString:[NSString stringWithFormat:@"%.2f$", revealShopPrice[i - 4]]];
        }
        return;
    }
    
    while(true) {
        int rand = arc4random()%[strpuzzleName length];
        
        BOOL isIn = NO;
        for(NSNumber *number in revealArray)
        {
            if([number intValue] == rand)
            {
                isIn = YES;
                break;
            }
        }
        if(isIn == YES)
            continue;
        
        [revealArray addObject:[NSNumber numberWithInt:rand]];
        
        CCMenuItemImage *spr = [puzzleBackArray objectAtIndex:rand];
        spr.visible = NO;
        
        CCLabelTTF *label = [puzzleLabelArray objectAtIndex:rand];
        [label setString:[strpuzzleName substringWithRange:NSMakeRange(rand, 1)]];
        label.color = ccc3(0,0,0);
        
        int num = [[inputedArray objectAtIndex:rand] intValue];
        
        currentPos++;
        
        if(num != -1)
        {
            [letterBtnMenu getChildByTag:num].visible = YES;
            CCLabelTTF *lbl = [letterLabelArray objectAtIndex:num];
            lbl.visible = YES;
            currentPos--;
        }
        
        for(int i = 0; i < [strpuzzleName length]; i++)
        {
            int nn = [[inputedArray objectAtIndex:i] intValue];
            if( nn!= -1 && [[indexArray objectAtIndex:nn] intValue] == rand)
            {
                if(rand == i)
                {
                    [letterBtnMenu getChildByTag:num].visible = NO;
                    CCLabelTTF *lbl = [letterLabelArray objectAtIndex:num];
                    lbl.visible = NO;
                    //currentPos--;
                    break;
                }
                
                [inputedArray replaceObjectAtIndex:i withObject:[NSNumber numberWithInt:-1]];
                
                CCLabelTTF *label = [puzzleLabelArray objectAtIndex:i];
                [label setString:@""];
                currentPos--;
                break;
            }
        }
        
        for(int i = 0; i < 14; i++)
        {
            if([[indexArray objectAtIndex:i] intValue] == rand)
            {
                [inputedArray replaceObjectAtIndex:rand withObject:[NSNumber numberWithInt:i]];
                
                [letterBtnMenu getChildByTag:i].visible = NO;
                CCLabelTTF *lbl = [letterLabelArray objectAtIndex:i];
                lbl.visible = NO;
                break;
            }
        }
        break;
    }
    if(currentPos < [strpuzzleName length]) {
        for(CCLabelTTF *lbl in puzzleLabelArray)
            lbl.color = ccc3(255,255,255);
        
        for(int i = 0; i < [revealArray count]; i++)
        {
            CCLabelTTF *lbl = [puzzleLabelArray objectAtIndex:[[revealArray objectAtIndex:i] intValue]];
            lbl.color = ccc3(0,0,0);
        }
    }
    else if(currentPos == [strpuzzleName length]) {
        NSString *str = @"";
        for(int i = 0; i < [strpuzzleName length]; i++)
        {
            str = [str stringByAppendingString:[letterArray objectAtIndex:[[inputedArray objectAtIndex:i] intValue]]];
        }
        
        if([str isEqualToString:strpuzzleName]) {
            [self showWinScreen];
        }
        else
        {
            if(sound == 0)
                [[SimpleAudioEngine sharedEngine] playEffect:@"fail.wav"];
            
            for(CCLabelTTF *lbl in puzzleLabelArray)
                lbl.color = ccc3(255,0,0);
            
            for(int i = 0; i < [revealArray count]; i++)
            {
                CCLabelTTF *lbl = [puzzleLabelArray objectAtIndex:[[revealArray objectAtIndex:i] intValue]];
                lbl.color = ccc3(0,0,0);
            }
        }
    }
    
    revealItemCount--;
    [revealCountLabel setString:[NSString stringWithFormat:@"%d", revealItemCount]];
    
    isSaved = 0;
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setInteger:isSaved forKey:@"isSaved"];
    [prefs setObject:revealArray forKey:@"revealArray"];
    [prefs setObject:inputedArray forKey:@"inputedArray"];
    [prefs setObject:letterArray forKey:@"letterArray"];
    [prefs setObject:indexArray forKey:@"indexArray"];
    [prefs setInteger:questionNumber forKey:@"number"];
    [prefs setInteger:skipItemCount forKey:@"skip"];
    [prefs setInteger:revealItemCount forKey:@"reveal"];
    [prefs setInteger:removeItemCount forKey:@"remove"];
    [prefs synchronize];
}


- (void)onSkipBtn {
    
    if(sound == 0)
        [[SimpleAudioEngine sharedEngine] playEffect:@"menu_button.wav"];
    
    if(skipItemCount == 0) {
        if(freeSkip == 1)
            [closeMenu getChildByTag:0].visible = NO;
        
        menuSkipShop.visible = YES;
        menuRemoveShop.visible = NO;
        menuRevealShop.visible = NO;
        shopLayer.visible = YES;
        
        [shopTitleLabel setString:@"Skip Puzzle"];
        
        for(int i = 0; i < 4; i++) {
            CCLabelTTF *lbl = [shopLabelArray objectAtIndex:i];
            [lbl setString:[NSString stringWithFormat:@"%d", skipShopCount[i]]];
        }
        
        for(int i = 4; i < 8; i++) {
            CCLabelTTF *lbl = [shopLabelArray objectAtIndex:i];
            [lbl setString:[NSString stringWithFormat:@"%.2f$", skipShopPrice[i - 4]]];
        }
        return;
    }
    
    
    // Promote for Rating & Add Frequency (Chartboost)
    
    if((questionNumber+1)%6 == 0) {
        [self ShowRateAlert];                                               // Show RATE ALERT
    }
    else {
        if((questionNumber+1)%ADS_FREQUENCY == 0) {
            
            [Chartboost showInterstitial:CBLocationHomeScreen];             // Show Chartboost Interstatial
            
        }
    }
    
    // Level Clear Layer
    
    if((questionNumber+1)%10 == 0) {
        
        levelClearLayer.visible = YES;
        
        [levelClearLabel setString:[NSString stringWithFormat:@"%d", (questionNumber/10) + 1]];
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [prefs setInteger:questionNumber+1 forKey:@"number"];
        [prefs synchronize];
    }
    else {
        questionNumber++;
        [self setLettersAndButtons:questionNumber :0];
        skipItemCount--;
        [skipCountLabel setString:[NSString stringWithFormat:@"%d", skipItemCount]];
    }
    
    NSString *puzzleNumber=[NSString stringWithFormat:@"Skipped %d Puzzle!",questionNumber+1];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setInteger:questionNumber forKey:@"number"];
    [prefs setInteger:skipItemCount forKey:@"skip"];
    [prefs setInteger:revealItemCount forKey:@"reveal"];
    [prefs setInteger:removeItemCount forKey:@"remove"];
    [prefs synchronize];
}


#pragma mark - social media sharing

- (void)onFacebookBtn {
    if(sound == 0)
        [[SimpleAudioEngine sharedEngine] playEffect:@"menu_button.wav"];
    
    NSString *imageName = [puzzleArray objectAtIndex:questionNumber];
    UIImage *gameImage = [UIImage imageNamed:[NSString stringWithFormat:@"%@.png",[imageName lowercaseString]]];
    
    [self shareToFacebookWithImage:gameImage
                              text:[NSString stringWithFormat:NSLocalizedString(@"socialShareMessageFacebook", nil), kUrlShortenedAppItune]
                              type:FacebookShareTypeShareImages];
    
    double delayInSeconds = 10.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
        revealItemCount+=2;
        [revealCountLabel setString:[NSString stringWithFormat:@"%d", revealItemCount]];
    });
}


- (void)onTweetBtn {
    if(sound == 0)
        [[SimpleAudioEngine sharedEngine] playEffect:@"menu_button.wav"];
    
    AppController* app = [AppController getInstance];
    TWTweetComposeViewController *tweetComposeViewController = [[TWTweetComposeViewController alloc] init];
    NSString *str = [puzzleArray objectAtIndex:questionNumber];
    [tweetComposeViewController addImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png",[str lowercaseString]]]];
    [tweetComposeViewController setInitialText:[NSString stringWithFormat:NSLocalizedString(@"socialShareMessageTwitter", nil), kUrlShortenedAppItune]];
    [tweetComposeViewController setCompletionHandler:
     ^(TWTweetComposeViewControllerResult result) {
         
         if(result) {
         }
         
         [app.navController dismissModalViewControllerAnimated:YES];
     }];
    [app.navController presentModalViewController:tweetComposeViewController animated:YES];
    
    double delayInSeconds = 10.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
        revealItemCount+=2;
        [revealCountLabel setString:[NSString stringWithFormat:@"%d", revealItemCount]];
    });
}


- (void)onInstagramBtn {
    if(sound == 0)
        [[SimpleAudioEngine sharedEngine] playEffect:@"menu_button.wav"];
    
    NSURL *instagramURL = [NSURL URLWithString:@"https://instagram.com/"];
    
    if([[UIApplication sharedApplication] canOpenURL:instagramURL]) {
        
        NSString *documentDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
        NSString *saveImagePath = [documentDirectory stringByAppendingPathComponent:@"Image.ig"];
        NSString *str = [puzzleArray objectAtIndex:questionNumber];
        NSData *imageData = UIImagePNGRepresentation([UIImage imageNamed:[NSString stringWithFormat:@"%@.png", [str lowercaseString]]]);
        [imageData writeToFile:saveImagePath atomically:YES];
        
        NSURL *imageURL = [NSURL fileURLWithPath:saveImagePath];
        
        UIDocumentInteractionController *docController = [[UIDocumentInteractionController alloc] init];
        docController.delegate = self;
        [docController retain];
        docController.UTI = @"com.instagram.photo";
        
        docController.annotation = [NSDictionary dictionaryWithObjectsAndKeys:@"Hey! I'm playing this NBA quiz game and stuck on this picture. Any idea who this Player might be? check this out and join the fun!", @"InstagramCaption", nil];
        
        [docController setURL:imageURL];
        
        AppController *appController = [AppController getInstance];
        [docController presentOpenInMenuFromRect:appController.navController.view.frame inView:appController.navController.view animated:YES];
        
        double delayInSeconds = 10.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
            revealItemCount+=2;
            [revealCountLabel setString:[NSString stringWithFormat:@"%d", revealItemCount]];
        });
    }
    
    else {
        NSLog(@"Instagram not found");
    }
}


#pragma mark - Design management


- (void)saveAll {
    
}


- (void)showWinScreen {
    
    // Interstatial ADD position
    
    if(questionNumber>1 && questionNumber%2==1){ [Chartboost showInterstitial:CBLocationHomeScreen];
        
    }
    
    if(questionNumber%2==0){ [[AdManager sharedInstance] showAdmobFullscreen];
        
    }
    if(questionNumber >= puzzleArray.count) {
        
        UIAlertView *view = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"atWellDone", nil)
                                                       message:NSLocalizedString(@"amPuzzleCompleted", nil)
                                                      delegate:self
                                             cancelButtonTitle:NSLocalizedString(@"yes", nil)
                                             otherButtonTitles:NSLocalizedString(@"no", nil), nil];
        [view show];
        [view release];
        return;
    }
    
    
    // Level Clear Layer
    
    if((questionNumber+1)%10 == 0) {
        
        if(sound == 0)
            [[SimpleAudioEngine sharedEngine] playEffect:@"level_win.wav"];
        
        levelClearLayer.visible = YES;      // show level clear screen
        
        [levelClearLabel setString:[NSString stringWithFormat:@"%d", (questionNumber/10) + 1]];
        
        skipItemCount+=0;
        revealItemCount+=0;
        removeItemCount+=0;
        
        [skipCountLabel setString:[NSString stringWithFormat:@"%d", skipItemCount]];
        [revealCountLabel setString:[NSString stringWithFormat:@"%d", revealItemCount]];
        [removeCountLabel setString:[NSString stringWithFormat:@"%d", removeItemCount]];
    }
    else {
        
        // Puzzel clear layer
        
        if(sound == 0)
            [[SimpleAudioEngine sharedEngine] playEffect:@"stage_win.wav"];
        
        winLayer.visible = YES;         // show Puzzle clear screen
        
        [foundpuzzleLabel setString:[puzzleArray objectAtIndex:questionNumber]];
        [puzzleNumberLabel setString:[NSString stringWithFormat:@"%d", questionNumber+1]];
    }
    
    
    // Show RATE ALERT
    
    if((questionNumber+1)%6 == 0) {
        
        [self ShowRateAlert];
        
        [iRate sharedInstance].applicationBundleID = kBundleID;
        [iRate sharedInstance].onlyPromptIfLatestVersion = YES;
        
        //enable preview mode
    }
    else {
        
        if((questionNumber+1)%ADS_FREQUENCY == 0){
            
            [Chartboost showInterstitial:CBLocationHomeScreen];         // Show Chartboost Interstatial
            
        }
        else {
            
        }
    }
    
    NSString *puzzleNumber=[NSString stringWithFormat:@"Solved %d Puzzle!",questionNumber+1];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setInteger:questionNumber+1 forKey:@"number"];
    [prefs setInteger:skipItemCount forKey:@"skip"];
    [prefs setInteger:revealItemCount forKey:@"reveal"];
    [prefs setInteger:removeItemCount forKey:@"remove"];
    [prefs synchronize];
}


- (void)setLettersAndButtons:(int)num :(int)flag {
    if(questionNumber >= puzzleArray.count) {
        
        UIAlertView *view = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"atWellDone", nil)
                                                       message:NSLocalizedString(@"amPuzzleCompleted", nil)
                                                      delegate:self
                                             cancelButtonTitle:NSLocalizedString(@"yes", nil)
                                             otherButtonTitles:NSLocalizedString(@"no", nil), nil];
        [view show];
        [view release];
        return;
    }
    
    deletedCount = 0;
    [levelLabel setString:[NSString stringWithFormat:@"Level %d", questionNumber / 10 + 1]];
    [questionLabel setString:[NSString stringWithFormat:@"%d", questionNumber+1]];
    
    if(puzzleSprite != nil) {
        [puzzleSprite removeFromParentAndCleanup:YES];
        puzzleSprite = nil;
    }
    
    NSString *str = [puzzleArray objectAtIndex:questionNumber];
    puzzleSprite = [CCSprite spriteWithFile:[NSString stringWithFormat:@"%@.png",[str lowercaseString]]];
    puzzleSprite.position = ccp(getX(384), getY(650));
    if(puzzleSprite.boundingBox.size.width > puzzleSprite.boundingBox.size.height) {
        puzzleSprite.scale = getX(420.0f) / puzzleSprite.boundingBox.size.width;
    }
    else {
        puzzleSprite.scale = getX(420.0f) / puzzleSprite.boundingBox.size.height;
    }
    [self addChild:puzzleSprite];
    
    [letterMenu removeFromParentAndCleanup:YES];
    
    for(CCLabelTTF *lbl in puzzleLabelArray)
        [lbl removeFromParentAndCleanup:YES];
    
    for(CCLabelTTF *lbl in signArray)
        [lbl removeFromParentAndCleanup:YES];
    
    [signArray removeAllObjects];
    [puzzleLabelArray removeAllObjects];
    [puzzleBackArray removeAllObjects];
    
    float startPosX; float letterWidth = getX(75.0f);
    float startPosY;
    
    if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        
        startPosY = getY(375);
    }
    else {
        
        startPosY = getY(380);
    }
    
    if([str length] > 9)
        startPosX = getX(384) - (letterWidth*4);
    else
        startPosX = getX(384) - (letterWidth* (([str length]-1) / 2.0f));
    
    letterMenu = [CCMenu menuWithItems: nil];
    
    int count = 0;
    for(int i = 0; i < [str length]; i++) {
        if(i == 9) {
            startPosY -= letterWidth;
            startPosX = getX(384)- (letterWidth* ([str length] - 10) / 2.0f);
        }
        NSString *letter = [str substringWithRange:NSMakeRange(i, 1)];
        
        if(![letter isEqualToString:@" "] && ![letter isEqualToString:@"-"]) {
            
            CCMenuItemImage *spr = [CCMenuItemImage itemWithNormalImage:@"letterback.png" selectedImage:@"letterback.png" target:self selector:@selector(onLetterBack:)];
            spr.position = ccp(startPosX + letterWidth*(i%9), startPosY);
            spr.tag = i - count;
            [letterMenu addChild:spr];
            [puzzleBackArray addObject:spr];
            
            CCLabelTTF *label = [CCLabelTTF labelWithString:@"" fontName:@"AmericanTypewriter-Bold" fontSize:getY(30)];
            label.position = ccp(startPosX + letterWidth*(i%9), startPosY);
            [self addChild:label z:1];
            [puzzleLabelArray addObject:label];
        }
        else {
            CCLabelTTF *label = [CCLabelTTF labelWithString:letter fontName:@"AmericanTypewriter-Bold" fontSize:getY(30)];
            label.position = ccp(startPosX + letterWidth*i, startPosY);
            label.color = ccc3(0,0,0);
            [self addChild:label z:1];
            [signArray addObject:label];
            count++;
        }
    }
    
    letterMenu.position = ccp(0,0);
    [self addChild:letterMenu];
    
    [letterArray removeAllObjects];
    
    str = [str stringByReplacingOccurrencesOfString:@"-" withString:@""];
    self.strpuzzleName = [str stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    for(int  i = 0; i < [str length]; i++) {
        NSString *letter = [str substringWithRange:NSMakeRange(i, 1)];
        if(![letter isEqualToString:@" "] && ![letter isEqualToString:@"-"])
            [letterArray addObject:[str substringWithRange:NSMakeRange(i, 1)]];
    }
    
    if([strpuzzleName length] < 14) {
        for(int i = 0; i < 14 - [strpuzzleName length]; i++)
        {
            int rand = arc4random() % 26;
            
            switch (rand) {
                case 0:
                    [letterArray addObject:@"B"];
                    break;
                case 1:
                    [letterArray addObject:@"C"];
                    break;
                case 2:
                    [letterArray addObject:@"D"];
                    break;
                case 3:
                    [letterArray addObject:@"F"];
                    break;
                case 4:
                    [letterArray addObject:@"G"];
                    break;
                case 5:
                    [letterArray addObject:@"H"];
                    break;
                case 6:
                    [letterArray addObject:@"J"];
                    break;
                case 7:
                    [letterArray addObject:@"K"];
                    break;
                case 8:
                    [letterArray addObject:@"L"];
                    break;
                case 9:
                    [letterArray addObject:@"M"];
                    break;
                case 10:
                    [letterArray addObject:@"N"];
                    break;
                case 11:
                    [letterArray addObject:@"P"];
                    break;
                case 12:
                    [letterArray addObject:@"Q"];
                    break;
                case 13:
                    [letterArray addObject:@"R"];
                    break;
                case 14:
                    [letterArray addObject:@"S"];
                    break;
                case 15:
                    [letterArray addObject:@"T"];
                    break;
                case 16:
                    [letterArray addObject:@"V"];
                    break;
                case 17:
                    [letterArray addObject:@"W"];
                    break;
                case 18:
                    [letterArray addObject:@"X"];
                    break;
                case 19:
                    [letterArray addObject:@"Y"];
                    break;
                case 20:
                    [letterArray addObject:@"Z"];
                    break;
                case 21:
                    [letterArray addObject:@"A"];
                    break;
                case 22:
                    [letterArray addObject:@"E"];
                    break;
                case 23:
                    [letterArray addObject:@"I"];
                    break;
                case 24:
                    [letterArray addObject:@"O"];
                    break;
                case 25:
                    [letterArray addObject:@"U"];
                    break;
                default:
                    break;
            }
        }
    }
    
    [indexArray removeAllObjects];
    for(int i = 0; i < 14; i++) {
        [indexArray addObject:[NSNumber numberWithInt:i]];
    }
    
    for(int i = 0; i < 30; i++) {
        int a = arc4random()%14;
        int b = arc4random()%14;
        [letterArray exchangeObjectAtIndex:a withObjectAtIndex:b];
        [indexArray exchangeObjectAtIndex:a withObjectAtIndex:b];
    }
    
    for(int  i = 0; i < 14; i++) {
        CCLabelTTF *label = [letterLabelArray objectAtIndex:i];
        [label setString:[letterArray objectAtIndex:i]];
        label.visible = YES;
    }
    
    [revealArray removeAllObjects];
    currentPos = 0;
    
    [inputedArray removeAllObjects];
    for(int i = 0; i < [strpuzzleName length]; i++)
        [inputedArray addObject:[NSNumber numberWithInt:-1]];
    
    for(int i = 0; i < 14; i++) {
        CCMenuItemImage *btn = (CCMenuItemImage*)[letterBtnMenu getChildByTag:i];
        btn.visible = YES;
    }
    
    CCMenuItemImage *btn = (CCMenuItemImage*)[menu getChildByTag:1];
    btn.isEnabled = YES;
    btn.opacity = 255;
}


#pragma mark - Product Clicked

- (void)onSkipShopBtns:(id)sender {
    CCMenuItemImage *btn = (CCMenuItemImage*)sender;
    
    if(btn.tag == 0) {
        if(isUploading == YES)
            return;
        
        NSSet * productIds;
        productIds = [NSSet setWithObject:Skip1];
        
        self.productRequest = [[[SKProductsRequest alloc] initWithProductIdentifiers:productIds] autorelease];
        productRequest.delegate = self;
        [productRequest start];
        
        uploadingView.hidden = YES;
        [uploadingView startAnimating];
        isUploading = YES;
    }
    else if(btn.tag == 1) {
        if(isUploading == YES)
            return;
        
        NSSet * productIds;
        productIds = [NSSet setWithObject:Skip2];
        
        self.productRequest = [[[SKProductsRequest alloc] initWithProductIdentifiers:productIds] autorelease];
        productRequest.delegate = self;
        [productRequest start];
        
        uploadingView.hidden = YES;
        [uploadingView startAnimating];
        isUploading = YES;
    }
    else if(btn.tag == 2) {
        if(isUploading == YES)
            return;
        
        NSSet * productIds;
        productIds = [NSSet setWithObject:Skip3];
        
        self.productRequest = [[[SKProductsRequest alloc] initWithProductIdentifiers:productIds] autorelease];
        productRequest.delegate = self;
        [productRequest start];
        
        uploadingView.hidden = YES;
        [uploadingView startAnimating];
        isUploading = YES;
    }
    else if(btn.tag == 3) {
        if(isUploading == YES)
            return;
        
        NSSet * productIds;
        productIds = [NSSet setWithObject:Skip4];
        
        self.productRequest = [[[SKProductsRequest alloc] initWithProductIdentifiers:productIds] autorelease];
        productRequest.delegate = self;
        [productRequest start];
        
        uploadingView.hidden = YES;
        [uploadingView startAnimating];
        isUploading = YES;
    }
}


- (void)onRevealShopBtns:(id)sender {
    CCMenuItemImage *btn = (CCMenuItemImage*)sender;
    
    if(btn.tag == 0) {
        if(isUploading == YES)
            return;
        
        NSSet * productIds;
        productIds = [NSSet setWithObject:Reveal1];
        
        self.productRequest = [[[SKProductsRequest alloc] initWithProductIdentifiers:productIds] autorelease];
        productRequest.delegate = self;
        [productRequest start];
        
        uploadingView.hidden = YES;
        [uploadingView startAnimating];
        isUploading = YES;
    }
    else if(btn.tag == 1) {
        if(isUploading == YES)
            return;
        
        NSSet * productIds;
        productIds = [NSSet setWithObject:Reveal2];
        
        self.productRequest = [[[SKProductsRequest alloc] initWithProductIdentifiers:productIds] autorelease];
        productRequest.delegate = self;
        [productRequest start];
        
        uploadingView.hidden = YES;
        [uploadingView startAnimating];
        isUploading = YES;
    }
    else if(btn.tag == 2) {
        if(isUploading == YES)
            return;
        
        NSSet * productIds;
        productIds = [NSSet setWithObject:Reveal3];
        
        self.productRequest = [[[SKProductsRequest alloc] initWithProductIdentifiers:productIds] autorelease];
        productRequest.delegate = self;
        [productRequest start];
        
        uploadingView.hidden = YES;
        [uploadingView startAnimating];
        isUploading = YES;
    }
    else if(btn.tag == 3) {
        if(isUploading == YES)
            return;
        
        NSSet * productIds;
        productIds = [NSSet setWithObject:Reveal4];
        
        self.productRequest = [[[SKProductsRequest alloc] initWithProductIdentifiers:productIds] autorelease];
        productRequest.delegate = self;
        [productRequest start];
        
        uploadingView.hidden = YES;
        [uploadingView startAnimating];
        isUploading = YES;
    }
}


- (void)onRemoveShopBtns:(id)sender {
    CCMenuItemImage *btn = (CCMenuItemImage*)sender;
    
    if(btn.tag == 0) {
        if(isUploading == YES)
            return;
        
        NSSet * productIds;
        productIds = [NSSet setWithObject:Remoev1];
        
        self.productRequest = [[[SKProductsRequest alloc] initWithProductIdentifiers:productIds] autorelease];
        productRequest.delegate = self;
        [productRequest start];
        
        uploadingView.hidden = YES;
        [uploadingView startAnimating];
        isUploading = YES;
    }
    else if(btn.tag == 1) {
        if(isUploading == YES)
            return;
        
        NSSet * productIds;
        productIds = [NSSet setWithObject:Remoev2];
        
        self.productRequest = [[[SKProductsRequest alloc] initWithProductIdentifiers:productIds] autorelease];
        productRequest.delegate = self;
        [productRequest start];
        
        uploadingView.hidden = YES;
        [uploadingView startAnimating];
        isUploading = YES;
    }
    else if(btn.tag == 2) {
        if(isUploading == YES)
            return;
        
        NSSet * productIds;
        productIds = [NSSet setWithObject:Remoev3];
        
        self.productRequest = [[[SKProductsRequest alloc] initWithProductIdentifiers:productIds] autorelease];
        productRequest.delegate = self;
        [productRequest start];
        
        uploadingView.hidden = YES;
        [uploadingView startAnimating];
        isUploading = YES;
    }
    else if(btn.tag == 3) {
        if(isUploading == YES)
            return;
        
        NSSet * productIds;
        productIds = [NSSet setWithObject:Remoev4];
        
        self.productRequest = [[[SKProductsRequest alloc] initWithProductIdentifiers:productIds] autorelease];
        productRequest.delegate = self;
        [productRequest start];
        
        uploadingView.hidden = YES;
        [uploadingView startAnimating];
        isUploading = YES;
    }
}


- (void)onShopCloseBtn {
    if(isUploading == YES)
        return;
    
    if(sound == 0)
        [[SimpleAudioEngine sharedEngine] playEffect:@"menu_button.wav"];
    
    [[AdManager sharedInstance] showAdmobFullscreen];
    
    shopLayer.visible = NO;
    
    [skipCountLabel setString:[NSString stringWithFormat:@"%d", skipItemCount]];
    [revealCountLabel setString:[NSString stringWithFormat:@"%d", revealItemCount]];
    [removeCountLabel setString:[NSString stringWithFormat:@"%d", removeItemCount]];
}


#pragma mark - In App Purchase Handling

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
    NSLog(@"request = %@", request.description);
    
    if( response == nil ) {
        [uploadingView stopAnimating];
        isUploading = NO;
        return;
    }
    
    if( response.products == nil ){
        [uploadingView stopAnimating];
        isUploading = NO;
        return;
    }
    
    NSLog(@"response.products = %@", response.products);
    if( [response.products count] == 0 ){
        [uploadingView stopAnimating];
        isUploading = NO;
        return;
    }
    
    self.product = [response.products lastObject];
    if( [SKPaymentQueue canMakePayments] == NO ) {
        [uploadingView stopAnimating];
        isUploading = NO;
        return;
    }
    
    
    SKPayment * payment = [SKPayment paymentWithProduct:self.product];
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}


#pragma mark - SKPaymentTransactionObserver

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions {
    for (SKPaymentTransaction *transaction in transactions) {
        
        switch (transaction.transactionState)
        {
            case SKPaymentTransactionStatePurchased: {
                uploadingView.hidden = NO;
                [uploadingView stopAnimating];
                isUploading = NO;
                //[purchasingSprite runAction:[CCScaleTo actionWithDuration:0.2f scale:0.0f]];
                //isPurchasing = NO;
                [self completeTransaction:transaction];
            }
                break;
            case SKPaymentTransactionStateFailed: {
                [self failedTransaction:transaction];
                // NSLog(@"%@-%@",transaction.payment.productIdentifier, transaction.error);
                //[purchasingSprite runAction:[CCScaleTo actionWithDuration:0.2f scale:0.0f]];
                //isPurchasing = NO;
                uploadingView.hidden = NO;
                [uploadingView stopAnimating];
                isUploading = NO;
            }
                break;
            case SKPaymentTransactionStateRestored:{
                [self restoreTransaction:transaction];
            }
            default:
                break;
        }
    }
}


- (void)failedTransaction:(SKPaymentTransaction *)transaction {
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
}

- (void) restoreTransaction:(SKPaymentTransaction *)transaction{
    
}


- (void) completeTransaction: (SKPaymentTransaction *)transaction {
    
    // Remove the transaction from the payment queue.
    
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
    
    if( [transaction.payment.productIdentifier isEqualToString:Skip1]) {
        skipItemCount += 3;
    }
    else if( [transaction.payment.productIdentifier isEqualToString:Skip2] ) {
        skipItemCount += 9;
    }
    else if( [transaction.payment.productIdentifier isEqualToString:Skip3] ) {
        skipItemCount += 20;
    }
    else if( [transaction.payment.productIdentifier isEqualToString:Skip4] ) {
        skipItemCount += 60;
    }
    else if( [transaction.payment.productIdentifier isEqualToString:Reveal1] ) {
        revealItemCount += 6;
    }
    else if( [transaction.payment.productIdentifier isEqualToString:Reveal2] ) {
        revealItemCount += 18;
    }
    else if( [transaction.payment.productIdentifier isEqualToString:Reveal3] ) {
        revealItemCount += 40;
    }
    else if( [transaction.payment.productIdentifier isEqualToString:Reveal4] ) {
        revealItemCount += 100;
    }
    else if( [transaction.payment.productIdentifier isEqualToString:Remoev1] ) {
        removeItemCount += 4;
    }
    else if( [transaction.payment.productIdentifier isEqualToString:Remoev2] ) {
        removeItemCount += 12;
    }
    else if( [transaction.payment.productIdentifier isEqualToString:Remoev3] ) {
        removeItemCount += 30;
    }
    else if( [transaction.payment.productIdentifier isEqualToString:Remoev4] ) {
        removeItemCount += 80;
    }
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setInteger:questionNumber forKey:@"number"];
    [prefs setInteger:skipItemCount forKey:@"skip"];
    [prefs setInteger:revealItemCount forKey:@"reveal"];
    [prefs setInteger:removeItemCount forKey:@"remove"];
    [prefs synchronize];
    
    [skipCountLabel setString:[NSString stringWithFormat:@"%d", skipItemCount]];
    [revealCountLabel setString:[NSString stringWithFormat:@"%d", revealItemCount]];
    [removeCountLabel setString:[NSString stringWithFormat:@"%d", removeItemCount]];
    
}


#pragma mark - Rate On The Appstore

-(void)Rate {
    
    if (([[[UIDevice currentDevice] systemVersion] compare:@"6.0" options:NSNumericSearch] != NSOrderedAscending)) {
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:[NSString stringWithFormat:RATE_LINK_IOS7]]];
    }
    else {
        
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:RATE_LINK_IOS6]];
    }
}


-(void)ShowRateAlert {
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    long rate = [prefs integerForKey:@"rate"];      //if application is opened first time
    
    // int rate = [prefs integerForKey:@"isItunesApproved"];
    // NSLog(@"rate:%d",rate);
    
    if(rate == 0) {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Reward !"
                                                            message:@" Rate 5* to unlock 50 reveals for Free !"
                                                           delegate:self
                                                  cancelButtonTitle:@"Unlock 50 Reveals"
                                                  otherButtonTitles:@"Remind me later",nil];
        alertView.tag = kTagRateAlert;
        [alertView show];
    }
}


#pragma mark - Alert View Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag == kTagInstagramAlert || alertView.tag == kTagFacebookAlert || alertView.tag == kTagTwitterAlert|| alertView.tag == kTagRateAlert) {
        
        if(alertView.tag== kTagRateAlert) {
            
            if(buttonIndex==0) {
                
                NSUserDefaults *prefs   = [NSUserDefaults standardUserDefaults];
                long reveal = [prefs integerForKey:@"reveal"] + 50;
                [prefs setInteger:reveal forKey:@"reveal"];
                
                [prefs setInteger:1 forKey:@"rate"];
                [prefs setInteger:questionNumber+1 forKey:@"number"];
                [prefs synchronize];
                
                double delayInSeconds = 5.0;
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
                    revealItemCount+=50;
                    [revealCountLabel setString:[NSString stringWithFormat:@"%d", revealItemCount]];
                });
                
                [self Rate];
            }
            else {
                //cancel clicked
            }
        }
        
        return;
    }
    if(buttonIndex == 0) {
        questionNumber = 0;
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [prefs setInteger:questionNumber forKey:@"number"];
        [prefs synchronize];
        [self onBackBtn];
    }
    else {
        [self onBackBtn];
    }
}

- (void) alert:(UIAlertView *) alert clickedButtonAtIndex:(NSInteger) index {
    if(index == 1) {
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [prefs setValue:@"rate" forKey:@"REVIEWED"];
        [prefs synchronize];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://itunes.apple.com/app/id1031048322"]];
    }
}

#pragma -mark ADMOB

- (void)adView:(GADBannerView *)view
didFailToReceiveAdWithError:(GADRequestError *)error{
    
    //NSLog(@"error is %@",error);
}

- (void)adViewDidReceiveAd:(GADBannerView *)view{
    
    [UIView beginAnimations:@"BannerSlide" context:nil];
    AbMob.frame = CGRectMake(0.0,
                             [[CCDirector sharedDirector] winSize].height -
                             AbMob.frame.size.height,
                             AbMob.frame.size.width,
                             AbMob.frame.size.height);
    [UIView commitAnimations];
    
    //NSLog(@"received ad");
}

@end
